use chrono::naive::NaiveTime;
use chrono::{DateTime, Duration, Local};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Medicine {
    /// Medicine name
    name: String,

    /// Medicine id
    id: String,

    /// Alarm hour and minute of the day
    alarm_time: NaiveTime,

    /// Interval of the snooze timer
    #[serde(with = "duration_serializer")]
    snooze_interval: Duration,
}

#[test]
fn test_from_json() {
    let input =
        r#"{"id": "id", "name": "Name", "alarm_time": "21:00:00", "snooze_interval": 1800}"#;
    let m: Medicine = serde_json::from_str(&input).unwrap();
    assert_eq!(m.id, "id");
    assert_eq!(m.name, "Name");
    assert_eq!(m.alarm_time, NaiveTime::from_hms(21, 0, 0));
    assert_eq!(m.snooze_interval, Duration::seconds(1800));
}

#[test]
fn test_to_json() {
    let m: Medicine = Medicine {
        id: "Id".to_string(),
        name: "Name".to_string(),
        alarm_time: NaiveTime::from_hms(1, 24, 32),
        snooze_interval: Duration::seconds(1800),
    };

    let expected = r#"{"name":"Name","id":"Id","alarm_time":"01:24:32","snooze_interval":1800}"#;

    assert_eq!(expected, serde_json::to_string(&m).unwrap());
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MedicineAlarm {
    /// Medicine to alert the user for
    medicine: Medicine,

    /// Next alarm absolute time
    alarm: DateTime<Local>,
}

impl std::fmt::Display for MedicineAlarm {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "({}, {})",
            self.medicine.name,
            self.alarm.format("%Y/%m/%d %H:%M")
        )
    }
}

impl MedicineAlarm {
    fn get_next_alarm(alarm_time: &NaiveTime) -> DateTime<Local> {
        let now = Local::now();
        let current = now.date().and_time(*alarm_time).expect("Invalid time");

        // keep today if in the future, otherwise jump one day in advance
        if current > now {
            current
        } else {
            now.date()
                .succ()
                .and_time(*alarm_time)
                .expect("Invalid time")
        }
    }

    pub fn get_name(&self) -> &str {
        &self.medicine.name
    }

    pub fn new(medicine: Medicine) -> MedicineAlarm {
        MedicineAlarm {
            alarm: MedicineAlarm::get_next_alarm(&medicine.alarm_time),
            medicine: medicine,
        }
    }

    /// Postpone the alert time of the specified duration
    pub fn snooze(&mut self) {
        self.alarm = self.alarm + self.medicine.snooze_interval;
    }

    /// Set the alarm for the next day
    pub fn reset(&mut self) {
        self.alarm = MedicineAlarm::get_next_alarm(&self.medicine.alarm_time);
    }

    /// Create an alert message if needed
    pub fn to_alert(&self) -> Option<String> {
        if self.alarm < Local::now() {
            Some(format!(
                "Prendi {}! /taken_{}",
                self.medicine.name, self.medicine.id
            ))
        } else {
            None
        }
    }

    pub fn matches(&self, id: String) -> bool {
        return self.medicine.id == id;
    }
}

/// Builder for Medicine struct
pub struct MedicineBuilder {
    pub id: Option<String>,
    pub name: Option<String>,
    pub alarm_time: Option<NaiveTime>,
    pub snooze_interval: Option<Duration>,
}

impl MedicineBuilder {
    pub fn new() -> MedicineBuilder {
        MedicineBuilder {
            id: None,
            name: None,
            alarm_time: None,
            snooze_interval: None,
        }
    }

    pub fn build(self) -> Result<Medicine, String> {
        Ok(Medicine {
            id: self.id.ok_or("Id not provided")?,
            name: self.name.ok_or("Name not provided")?,
            alarm_time: self.alarm_time.ok_or("Alarm time not provided")?,
            snooze_interval: self.snooze_interval.ok_or("Snooze interval not provided")?,
        })
    }
}

mod duration_serializer {
    use chrono::Duration;
    use serde::{self, Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(duration: &Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_i64(duration.num_seconds())
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Duration, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Duration::seconds(i64::deserialize(deserializer)?))
    }
}
