extern crate serde;

use futures;
use futures::StreamExt;
use log::{debug, info};
use std::env;
use std::error::Error;
use std::sync::Mutex;
use telegram_bot::CanSendMessage;
use telegram_bot::{
    Api, CanReplySendMessage, Message, MessageKind, SendMessage, UpdateKind, UserId,
};

mod model;
use crate::model::{Medicine, MedicineAlarm};

async fn remind_alarms(
    bot: &Api,
    bot_owner: &UserId,
    medicines_alarms: &Mutex<Vec<MedicineAlarm>>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    loop {
        tokio::time::delay_for(tokio::time::Duration::from_secs(60)).await;

        let mut medicine_alarms = medicines_alarms
            .lock()
            .map_err(|err| format!("Unable to gain MEDICINES_ALARMS lock: {}", err))?;

        for medicine_alarm in medicine_alarms.iter_mut() {
            if let Some(msg) = medicine_alarm.to_alert() {
                bot.send(bot_owner.text(&msg)).await?;
                medicine_alarm.snooze();

                info!("Triggered alarm: {}", msg);
            } else {
                #[cfg(debug_assertions)]
                debug!("No alarms for {}", medicine_alarm);
            }
        }
    }
}

#[tokio::main(core_threads = 2)]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    env_logger::init();

    let bot: Api = {
        let token = env::var("BOT_TOKEN").expect("BOT_TOKEN not set");
        Api::new(token)
    };

    let bot_owner: UserId = {
        let user_id = env::var("USER").expect("USER not set");
        UserId::new(user_id.parse::<i64>().expect("Invalid USER"))
    };

    let medicines_alarms: Mutex<Vec<MedicineAlarm>> = {
        let raw_medicines = env::var("MEDICINES")?;
        let medicines: Vec<Medicine> = serde_json::from_str(&raw_medicines)?;

        let mut alarms = vec![];
        for m in medicines {
            alarms.push(MedicineAlarm::new(m));
        }
        Mutex::new(alarms)
    };

    let bot_task = check_telegram_messages(&bot, &bot_owner, &medicines_alarms);
    let alarm_task = remind_alarms(&bot, &bot_owner, &medicines_alarms);

    let (result_msg, result_alarms) = futures::join!(bot_task, alarm_task);
    result_msg.expect("check_telegram_messages failure");
    result_alarms.expect("remind_alarms failure");

    Ok(())
}

async fn check_telegram_messages(
    bot: &Api,
    _bot_owner: &UserId,
    medicines_alarms: &Mutex<Vec<MedicineAlarm>>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let mut stream = bot.stream();
    while let Some(update) = stream.next().await {
        let update = update?;
        if let UpdateKind::Message(message) = update.kind {
            if let Some(msg) = get_response(&message, medicines_alarms) {
                debug!("Answering {:?}", msg);

                bot.send(msg).await?;
            } else {
                debug!("Unsupported content received");
            }
        }
    }

    Ok(())
}

fn get_response<'a>(
    message: &'a Message,
    medicines_alarms: &'a Mutex<Vec<MedicineAlarm>>,
) -> Option<SendMessage<'a>> {
    if let MessageKind::Text { ref data, .. } = message.kind {
        if data == "/hi" {
            let text = format!("Hi, {}! {}", &message.from.first_name, &message.from.id);
            Some(message.text_reply(text))
        } else if data.starts_with("/taken_") {
            let medicine_id = &data[7..];

            let mut alarms = medicines_alarms
                .lock()
                .expect("Unable to gain MEDICINES_ALARMS lock");

            for alarm in alarms.iter_mut() {
                if alarm.matches(medicine_id.into()) {
                    alarm.reset();
                    return Some(message.text_reply(format!(
                        "Preso {}, timer spostato a domani",
                        alarm.get_name()
                    )));
                }
            }

            Some(message.text_reply(format!("Unknown medicine {}", medicine_id)))
        } else {
            None
        }
    } else {
        None
    }
}
